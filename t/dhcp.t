use Test::More tests => 5;

use_ok "Rex::IO::DHCP::Agent::Helper::DHCP::ISC";


my $dhcp = Rex::IO::DHCP::Agent::Helper::DHCP::ISC->new;
$dhcp->read_lease_file("t/dhcpd.lease");

ok($dhcp->get_mac_from_ip("192.168.7.150") eq "00:16:76:e2:e9:ef", "got mac from static host");
ok($dhcp->get_mac_from_ip("192.168.7.201") ne "01:16:76:e2:e9:ef", "first lease overwritten");
ok($dhcp->get_mac_from_ip("192.168.7.201") eq "02:16:76:e2:e9:ef", "got mac from lease entry");

$dhcp = Rex::IO::DHCP::Agent::Helper::DHCP::ISC->new;
$dhcp->read_lease_file("t/dhcpd.lease.2");

ok($dhcp->get_mac_from_ip("192.168.7.41") eq "52:54:00:10:12:f0", "got mac from static host (2)");

