#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
=head1 NAME

Rex::IO::DHCP::Agent - Webservice to query and manipulate DHCP Servers

=head1 DESCRIPTION

This Webservice is part of Rex.IO. This services is needed to query and manipulate DHCP Servers. Right now only ISC DHCP is supported.

=head1 GETTING HELP

=over 4

=item * IRC: irc.freenode.net #rex

=item * Wiki: L<https://github.com/krimdomu/rex-io-dhcp-agent/wiki>

=item * Bug Tracker: L<https://github.com/krimdomu/rex-io-dhcp-agent/issues>

=back

=head1 INSTALLATION

Rex::IO::DHCP::Agent must be installed on your DHCP server. Because it needs to read the lease file on the filesystem.

You can install it via the known CPAN tools like cpanminus or cpanplus.

After the installation you have to create a configuration file in one of the following locations:

=over 4

=item * /etc/rex/io/dhcp-agent.conf

=item * /usr/local/etc/rex/io/dhcp-agent.conf

=back

This is an example configuration file:

 {
   dhcp_server => "192.168.1.6",
   lease_file => "/var/state/dhcp/dhcpd.leases",
 }

To allow the service to create new leases you have to configure your server to allow omapi protocol.

Add the following line into your dhcpd.conf file and restart the service.

 omapi-port 7911;

It is possible to secure omapi with keys, but this isn't supported yet. You can block incoming traffic on port 7911 with iptables.

You can download an init script from the source repository at github. L<https://github.com/krimdomu/rex-io-dhcp-agent/>.

=cut

package Rex::IO::DHCP::Agent;
use Mojo::Base 'Mojolicious';

our $VERSION = "0.0.1";

# This method will run once at server start
sub startup {
   my $self = shift;

   # Documentation browser under "/perldoc"
   #$self->plugin('PODRenderer');

   my @cfg = ("/etc/rex/io/dhcp-agent.conf", "/usr/local/etc/rex/io/dhcp-agent.conf", "dhcp-agent.conf");
   my $cfg;
   for my $file (@cfg) {
      if(-f $file) {
         $cfg = $file;
         last;
      }
   }
   $self->plugin('Config', file => $cfg);



   # Router
   my $r = $self->routes;

   # Normal route to controller
   $r->route("/")->via("LIST")->to("main#list_leases");
   $r->post("/:mac")->to("main#create_lease");
   $r->get("/mac/:ip")->to("main#get_mac_from_ip");

   $r->get("/")->to("main#index");
}

1;
