#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::DHCP::Agent::Helper::DHCP;

use strict;
use warnings;

sub create {
   my ($class, $type, %option) = @_;

   $type ||= "ISC";

   my $klass = "Rex::IO::DHCP::Agent::Helper::DHCP::$type";

   eval "use $klass";
   return $klass->new(%option);
}

1;
