#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::DHCP::Agent::Helper::DHCP::ISC;

use strict;
use warnings;

use Data::Dumper;
use IPC::Open2;

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = { @_ };

   bless($self, $proto);

   return $self;
}

sub read_lease_file {
   my ($self, $file) = @_;

   unless(-f $file) {
      die "File: $file not found.";
   }

   my $content = eval { local(@ARGV, $/) = ($file); <>; };
   
   my @leases_dir = $content =~ m/lease (\d+\.\d+\.\d+\.\d+)/gs;
   my %leases = $content =~ m/lease (\d+\.\d+\.\d+\.\d+) \{\n([^\}]+)\}\n/gs;

   my @hosts_dir = $content =~ m/host ([a-zA-Z0-9\-\.]+)/gs;
   my %hosts  = $content =~ m/host ([a-zA-Z0-9\-\.]+) \{\n([^\}]+)\}\n/gs;

   for my $ip (@leases_dir) {
      $self->{__data__}->{$ip} = $self->parse_lease_body($leases{$ip});
   }

   for my $host (@hosts_dir) {
      my ($ip, $data) = $self->parse_host_body($hosts{$host});
      if(! $ip) { next; }

      $self->{__data__}->{$ip} = $data;
   }
}

sub get_leases {
   my ($self) = @_;
   return $self->{__data__};
}

sub parse_lease_body {
   my ($self, $body) = @_;

   my $ret = {};

   for my $line (split(/\n/, $body)) {
      $line =~ s/^\s*//;
      $line =~ s/\s*$//;

      if($line =~ m/^hardware ethernet (..:..:..:..:..:..);$/) {
         $ret->{"mac"} = $1;
      }
   }

   return $ret;
}

sub parse_host_body {
   my ($self, $body) = @_;

   my $ret = {};
   my $ip;

   for my $line (split(/\n/, $body)) {
      $line =~ s/^\s*//;
      $line =~ s/\s*$//;

      if($line eq "deleted;") { return; }

      if($line =~ m/^fixed\-address (\d+\.\d+\.\d+\.\d+);$/) {
         $ip = $1;
         next;
      }

      if($line =~ m/^hardware ethernet (..:..:..:..:..:..);$/) {
         $ret->{"mac"} = $1;
      }
   }

   return ($ip, $ret);
}

sub get_mac_from_ip {
   my ($self, $ip) = @_;
   return $self->{__data__}->{$ip}->{mac};
}

sub delete_lease_of_host {
   my ($self, $host) = @_;

   $self->_call("connect",
                "new host",
                "set name = \"$host\"",
                "open",
                "remove");
}

sub create_lease_for_mac {
   my ($self, %option) = @_;

   $self->delete_lease_of_host($option{name});

   $self->_call("connect",
                "new host",
                "set name = \"" . $option{name} . "\"",
                "set hardware-address = " . $option{mac},
                "set hardware-type = 1",
                "set ip-address = " . $option{ip},
                #"set client-hostname = \"" . $option{name} . "\"",
                "create");
}

sub _call {
   my ($self, @cmd) = @_;

   my ($out, $in);
   open2($out, $in, "omshell");

   print $in "server " . $self->{server} . "\n";

   my $ret = {};
   for (@cmd) {
      print $in "$_\n";
   }

   close($in);

   my $out_str = do {
      local $/;
      <$out>;
   };

   $self->{app}->log->debug($out_str);

   return split(/\n/, $out_str);
}


1;
