package Rex::IO::DHCP::Agent::Main;
use Mojo::Base 'Mojolicious::Controller';

use Mojo::JSON;
use Rex::IO::DHCP::Agent::Helper::DHCP;

my $dhcp;

sub index {
   my ($self) = @_;
   $self->render_text("Rex::IO::Agent::DHCP");
}

sub list_leases {
   my ($self) = @_;

   $self->_dhcp->read_lease_file($self->config->{"lease_file"});

   $self->render_json({ok => Mojo::JSON->true, leases => $self->_dhcp->get_leases});
}

# CALL:
# curl -X POST -d '{"name":"somehostname","ip":"1.2.3.4"}' http://localhost:4000/11:22:33:44:55:66
sub create_lease {
   my ($self) = @_;
   my $json = $self->req->json;

   $self->_dhcp->create_lease_for_mac(
      mac => $self->stash("mac"),
      name => $json->{name},
      ip => $json->{ip},
   );

   return $self->render_json({ok => Mojo::JSON->true});
}

sub get_mac_from_ip {
   my ($self) = @_;
   $self->_dhcp->read_lease_file($self->config->{"lease_file"});

   my ($ip) = ($self->req->url =~ m/^.*\/(.*?)$/);

   my $mac = $self->_dhcp->get_mac_from_ip($ip);

   if($mac) {
      return $self->render_json({ok => Mojo::JSON->true, mac => $mac});
   }
   else {
      return $self->render_json({ok => Mojo::JSON->false}, status => 404);
   }
}

sub _dhcp {
   my ($self) = @_;
   
   unless($dhcp) {
      $dhcp = Rex::IO::DHCP::Agent::Helper::DHCP->create("ISC", server => $self->config->{"dhcp_server"}, app => $self->app);
   }

   return $dhcp;
}

1;
